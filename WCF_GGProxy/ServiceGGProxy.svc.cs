﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCF_GGProxy
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ServiceProxy : IServiceGGProxy
    {
        WS_GGCommunity.GUISoapClient ggcommunity;

        public ServiceProxy() {
            ggcommunity = new WS_GGCommunity.GUISoapClient("GUISoap12");
        }

        public string GetDataById(string username, string password, string dataview, string id, string field)
        {
            DataSet dsResp;
            
            try
            {
                switch (dataview)
                {
                    case "species":
                        {
                            dsResp = ggcommunity.GetData(true, username, password, "get_taxonomy_species", ":taxonomyspeciesid = " + id + "; :accessionid =; :inventoryid =; :taxonomygenusid =; :cooperatorid =; :geographyid =; :cropid =;", 0, 15000, null);
                            if (dsResp != null && dsResp.Tables.Contains("get_taxonomy_species") && dsResp.Tables["get_taxonomy_species"].Rows.Count > 0)
                            {
                                return dsResp.Tables["get_taxonomy_species"].Rows[0].Field<string>(field);
                            }
                            break;
                        }
                    case "accession":
                        {
                            dsResp = ggcommunity.GetData(true, username, password, "get_accession", ":accessionid=" + id + "; :taxonomyspeciesid=; :inventoryid=; :orderrequestid=; :geographyid=; :cooperatorid=; :taxonomygenusid=; ", 0, 15000, null);
                            if (dsResp != null && dsResp.Tables.Contains("get_accession") && dsResp.Tables["get_accession"].Rows.Count > 0)
                            {
                                return dsResp.Tables["get_accession"].Rows[0].Field<string>(field);
                            }
                            break;
                        }
                    case "login":
                        {
                            dsResp = ggcommunity.ValidateLogin(false, username, password);
                            if (dsResp != null && dsResp.Tables.Contains("validate_login") && dsResp.Tables["validate_login"].Rows.Count > 0)
                            {
                                return "Login Successful";
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "Error: Data dont exist";
        }

    }
}
