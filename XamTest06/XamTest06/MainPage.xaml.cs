﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;

namespace XamTest06
{
    public partial class MainPage : ContentPage
    {
        WCF_GGProxy.ServiceGGProxyClient wsGRINGlobal;

        public MainPage()
        {
            InitializeComponent();
            
            wsGRINGlobal = new WCF_GGProxy.ServiceGGProxyClient();
        }

        private void ButtonInvoke_Clicked(object sender, EventArgs e)
        {
            LabelMessage.Text = "...";

            wsGRINGlobal.GetDataByIdCompleted += A_GetDataByIdCompleted;
            wsGRINGlobal.GetDataByIdAsync(EntryUsername.Text, EntryPassword.Text, EntryDataview.Text, EntryID.Text, EntryField.Text);
            
        }

        private void A_GetDataByIdCompleted(object sender, WCF_GGProxy.GetDataByIdCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    LabelMessage.Text = e.Result;
                });
            }
            else {
                Device.BeginInvokeOnMainThread(() =>
                {
                    LabelMessage.Text = e.Error.ToString();
                });
            }
        }
    }
    
}
